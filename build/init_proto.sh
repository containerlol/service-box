#!/bin/sh

#root 权限检测
function root_need() {
    if [[ $EUID -ne 0 ]]; then
        echo "Error:This script must be run as root!" 1>&2
        exit 1
    fi
}

#前置环境安装，编译需要旧环境
function initenv() {
    echo ============init protoc================
    yum install libtool -y 
    yum install autoconf -y 
}

function down_protobuf(){
    echo "protobuff 开始下载 版本3.11.0"
    wget https://github.com/protocolbuffers/protobuf/archive/v3.11.0.tar.gz
    if [ $? -gt 0 ]
    then
        echo "下载失败请手动下载protobuf 在执行脚本"
        exit -1
    fi
}

function install_proto(){
    echo "开始解压"
    if [ ! -d ./protobuf-3.11.0 ]
    then
        tar zxvf v3.11.0.tar.gz
        if [ $? -gt -0 ]
        then
            echo "解压失败 请检查文件是否正确"
            exit 1
        fi 
    fi

    cd protobuf-3.11.0

    ./autogen.sh

    if [ ! -d ./build ]
    then
        mkdir build 
    fi 

    cd build
    echo "开始配置 ../configure --prefix=/usr/local/protobuf --enable-static CXXFLAGS=-fPIC"
    ../configure --prefix=/usr/local/protobuf --enable-static CXXFLAGS="-fPIC"

    if [ $? -gt 0 ]
    then
        echo "--prefix=/usr/local/protobuf --enable-static CXXFLAGS=-fPIC 执行失败，检查log文件"
        exit 1
    fi 

    make -j4
    make install 

    if [ $? -gt 0 ]
    then
        echo "安装失败"
        exit 1
    fi 

    echo "处理软连接"
    if [ -f /usr/local/bin/protoc ]
    then 
        rm /usr/local/bin/protoc
    fi 
    ln -s /usr/local/protobuf/bin/protoc /usr/local/bin/protoc

    #这里比较愚蠢所有google的东西include 都在一个目录下，所有这里需要特殊判断一下 不能莽直接删！！！！！
    if [ ! -d /usr/local/include/google ];then
        mkdir /usr/local/include/google
    fi

    if [ -f /usr/local/include/google/protobuf ];then
        rm /usr/local/include/google/protobuf
    fi 
    ln -sv /usr/local/protobuf/include/google/protobuf /usr/local/include/google/protobuf 
    # 库文件
    if [ -f /usr/local/lib/libprotobuf.a ];then
        rm /usr/local/lib/libprotobuf.a
    fi 
    ln -s /usr/local/protobuf/lib/libprotobuf.a /usr/local/lib/libprotobuf.a
}

#检查目录
echo "请使用root用户运行此脚本"
root_need

#检查gcc 如果安装了就不装
if [ -x "$(command -v protoc)" ]; then
### get version code
echo "已经安装protoc 请自行确定版本是否正确 最小版本 3.10"
echo `protoc --version`
exit 0
fi

#init 
initenv

echo "开始下载proto 文件"
if [ ! -d /data ]
then
    echo "data 目录不存在"
    mkdir /data
    chmod 777 data
fi

if [ ! -f /data/thirdparty ]
then
    echo "建立下载缓存 /data/thirdparty"
    mkdir /data/thirdparty
fi

# download gcc from thirparty
cd /data/thirdparty 

if [ ! -f v3.11.0.tar.gz ]
then
    down_protobuf
fi

install_proto