#!/bin/sh

yum install redis -y
chown redis:redis /var/log/redis/redis.log

yum install nginx -y

cd service-box-linux-dependency
cp google /usr/local/include/ -rf
cp hiredis /usr/local/include/ -rf
cp libcurl.a /usr/local/lib/libcurl.a -rf
cp libhashtable.a /usr/local/lib/libhashtable.a -rf
cp libhiredis.a /usr/local/lib/libhiredis.a -rf
cp libz.a /usr/local/lib/libz.a  -rf
cp libzookeeper.a /usr/local/lib/libzookeeper.a -rf


cd ../../src/repo
python repo.py -t cpp -i
python repo.py -t cpp -a example/example.idl
python repo.py -t cpp -b example
python repo.py -t lua -i
python repo.py -t lua -u example
python repo.py -t lua -b example

cd ..
cmake .
make -j4

cd ../unittest/src
cmake .
make -j4
