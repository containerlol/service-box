cd ..\src\repo
@ call:func -t cpp -i
cd ..\..\win-proj
"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild" service-box.sln -target:service-box
"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild" service-box.sln -target:service-box-unittest
cd ..\src\repo
@ call:func -t cpp -a example\example.idl
@ call:func -t cpp -b example
@ call:func -t lua -i
@ call:func -t lua -u example
@ call:func -t lua -b example

copy /y lib\stub\example\ServiceDynamic\Debug\libServiceDynamic.so ..\..\build\service-box-windows-dependency\nginx\html\doc

pause
exit

@ :func
  python repo.py %1 %2 %3 %4 %5 %6 %7 %8 %9
@ if %errorlevel% == 0 ( echo "Success!" ) else ( echo "Build Faild !" & pause )
@ goto:eof
