#!/bin/sh

if [ ! -d /data ]
then
    echo "data 目录不存在"
    mkdir /data
    chmod 777 data
fi

if [ ! -d /data/thirdparty ]
then
    echo "建立下载缓存 /data/thirdparty"
    mkdir /data/thirdparty
fi

yum install -y wget python2 bzip2 java dos2unix
ln /usr/bin/python2 /usr/bin/python

#change source to aliyun 
#echo "backpack source file"
#mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.back
#download source
#wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
#yum makecache

yum install -y git

python build_local_env_linux.py
cp ./service-box-linux-dependency/v3.11.0.tar.gz /data/thirdparty/v3.11.0.tar.gz -f

dos2unix *

chmod +x ./initgcc.sh
chmod +x ./build_repo_linux.sh
chmod +x ../tools/start_linux_box.sh
chmod +x ../tools/start_linux_local_env.sh
chmod +x ../tools/stop_linux_local_env.sh

dos2unix ./service-box-linux-dependency/zookeeper-bin/bin/*
chmod +x ./service-box-linux-dependency/zookeeper-bin/bin/zkServer.sh

#如果文件存在但是没有生效的话 需要再这里source 一下 把环境刷新到这个bash里面
if [ -f /etc/profile.d/gcc.sh ];then
    source /etc/profile.d/gcc.sh
fi 

./initgcc.sh
source /etc/profile.d/gcc.sh


if [ $? -gt 0 ]
then
        echo "install gcc init error"
        exit 1
fi 

sh init_proto.sh

if [ $? -gt 0 ]
then
        echo "install protobuf error"
        exit 1
fi 

./build_repo_linux.sh
