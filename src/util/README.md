# Utility工具

ServiceBox开发所使用的工具库

## 功能列表

1. [assert函数宏](https://gitee.com/dennis-kk/service-box/blob/master/src/util/box_debug.hh)
2. [STL Allocator](https://gitee.com/dennis-kk/service-box/blob/master/src/util/box_std_allocator.hh)
3. [帧延时类](https://gitee.com/dennis-kk/service-box/blob/master/src/util/frame_delayer.hh)
4. [对象池](https://gitee.com/dennis-kk/service-box/blob/master/src/util/object_pool.hh)
5. [OS相关工具函数](https://gitee.com/dennis-kk/service-box/blob/master/src/util/os_util.hh)
6. [单例模板](https://gitee.com/dennis-kk/service-box/blob/master/src/util/singleton.hh)
7. [单生产者单消费者无锁队列](https://gitee.com/dennis-kk/service-box/blob/master/src/util/spsc_queue.hpp)
8. [字符串相关工具函数](https://gitee.com/dennis-kk/service-box/blob/master/src/util/string_util.hh)
9. [时间系统](https://gitee.com/dennis-kk/service-box/blob/master/src/util/time_system.hh)
10. [时间相关工具函数](https://gitee.com/dennis-kk/service-box/blob/master/src/util/time_system.hh)
11. [定时器树](https://gitee.com/dennis-kk/service-box/blob/master/src/util/timer_tree.hh)
12. [单轮时间轮定时器](https://gitee.com/dennis-kk/service-box/blob/master/src/util/timer_wheel.hh)
13. [支持延帧回收、支持多线程环境下生命周期管理的对象系统](https://gitee.com/dennis-kk/service-box/blob/master/src/util/object.hh)
14. [lua虚拟机、lua服务、lua调试器](https://gitee.com/dennis-kk/service-box/tree/master/src/util/lua)
15. [WebSocket Server](https://gitee.com/dennis-kk/service-box/tree/master/src/util/websocket)
16. [对象系统](https://gitee.com/dennis-kk/service-box/blob/master/src/util/README-object.md)