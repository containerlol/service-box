#pragma once

#include <string>

#define VERSION_MAJOR 1 // 主版本号
#define VERSION_MINOR 0 // 次版本号
#define VERSION_PATCH 12 // 补丁

/**
 * 获取版本号.
 * 
 * \return 版本号
 */
static inline std::string get_version_string() {
  return "version " +
      std::to_string(VERSION_MAJOR) +
      "." +
      std::to_string(VERSION_MINOR) +
      "." +
      std::to_string(VERSION_PATCH) + "-alpha";
}
