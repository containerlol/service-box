// Machine generated code

#include <utility>
#include "rpc_singleton.h"
#include "object_pool.h"
#include "example.service.Service.impl.h"

ServiceImpl::ServiceImpl() {}
ServiceImpl::~ServiceImpl() {}
bool ServiceImpl::onAfterFork(rpc::Rpc* rpc) { return true; }
bool ServiceImpl::onBeforeDestory(rpc::Rpc* rpc) { return true; }
void ServiceImpl::onTick(std::time_t ms) {}
void ServiceImpl::method1(rpc::StubCallPtr call, const Data&, const std::string&) {
}

std::shared_ptr<std::string> ServiceImpl::method2(rpc::StubCallPtr call, std::int8_t, const std::unordered_set<std::string>&, std::uint64_t) {
    return rpc::make_shared_ptr<std::string>();
}

std::shared_ptr<std::unordered_map<std::int64_t,Dummy>> ServiceImpl::method3(rpc::StubCallPtr call, std::int8_t, const std::unordered_set<std::string>&, const std::unordered_map<std::int64_t,std::string>&, std::uint64_t) {
    return rpc::make_shared_ptr<std::unordered_map<std::int64_t,Dummy>>();
}

void ServiceImpl::method4(rpc::StubCallPtr call) {
}

void ServiceImpl::method5(rpc::StubCallPtr call) {
}

