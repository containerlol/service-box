// Machine generated code

using System.Collections.Generic;
using kratos;

namespace rpc {

//interface->
internal abstract class Service : IService {
    uint id_ = 0;
    public ulong UUID { get { return 5871407834219999997; } }
    public uint ID { get { return id_; } set { id_ = value; } }
    public virtual void OnTick(long ms) {}
    public abstract void method1(Data arg1,string arg2);
    public abstract string method2(sbyte arg1,HashSet<string> arg2,ulong arg3);
    public abstract Dictionary<long,Dummy> method3(sbyte arg1,HashSet<string> arg2,Dictionary<long,string> arg3,ulong arg4);
    public abstract void method4();
    public abstract void method5();
}

//interface<-
}

