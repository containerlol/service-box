#pragma once

#include "example.service.Lua.h"
#include "service_context.hh"

class LuaImpl : public Lua {
  kratos::service::LuaServicePtr lua_service_;
  kratos::service::BoxConsolePtr console_;
  kratos::service::LoggerPtr logger_;
  bool is_debugger_opened_{false};
  bool is_debugger_enable_{false};

public:
//implementation->
    LuaImpl();
    virtual ~LuaImpl();
    virtual bool onAfterFork(rpc::Rpc* rpc) override;
    virtual bool onBeforeDestory(rpc::Rpc* rpc) override;
    virtual void onTick(std::time_t ms) override;
    virtual void onServiceCall(rpc::StubCallPtr callPtr) override;
//implementation<-
};

