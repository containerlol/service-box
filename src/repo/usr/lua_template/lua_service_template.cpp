// Machine generated code

#include "example.service.Lua.impl.h"
#include "object_pool.h"
#include "rpc_root.h"
#include "rpc_singleton.h"
#include <string>
#include <utility>

#include "../util/lua/lua_service.hh"
#include "../console/box_console.hh"

LuaImpl::LuaImpl() {}

LuaImpl::~LuaImpl() {}

bool LuaImpl::onAfterFork(rpc::Rpc *rpc) {
  logger_ = getContext()->get_module_logger(std::to_string(getServiceUUID()) + "_lua");
  lua_service_ = getContext()->new_lua_service();
  if (!lua_service_->start(getServiceUUID())) {
    return false;
  }
  console_ = getContext()->new_box_console(std::to_string(getServiceUUID()));
  console_->add_switch(std::to_string(getServiceUUID()) + "_debugger",
  {
      [&](kratos::console::Console& console, kratos::console::ConsoleSwitch& cs)->void {
         cs.set_display_name(std::to_string(getServiceUUID()) + "_debugger");
         cs.set_on_off(is_debugger_opened_);
         cs.set_tips("Open lua debugger");
      },
      [&](kratos::console::Console& console, bool on_off, std::string& result)->bool {
         if (!is_debugger_opened_) {
           lua_service_->open_debugger(std::to_string(getServiceUUID()));
         } else {
           lua_service_->close_debugger();
         }
         is_debugger_opened_ = !is_debugger_opened_;
         return true;
      }
  });
  console_->add_switch(std::to_string(getServiceUUID()) + "_enable_debugger",
  {
      [&](kratos::console::Console& console, kratos::console::ConsoleSwitch& cs)->void {
         cs.set_display_name(std::to_string(getServiceUUID()) + "_enable_debugger");
         cs.set_on_off(is_debugger_enable_);
         cs.set_tips("Enable or disable lua debugger");
      },
      [&](kratos::console::Console& console, bool on_off, std::string& result)->bool {
         if (!is_debugger_enable_) {
           lua_service_->enable_debugger();
         } else {
           lua_service_->disable_debugger();
         }
         is_debugger_enable_ = !is_debugger_enable_;
         return true;
      }
  });
  logger_->info_log("Lua service started");
  return true;
}

bool LuaImpl::onBeforeDestory(rpc::Rpc *rpc) {  
  if (!lua_service_->stop()) {
    logger_->info_log("Stop lua service failed");
    return false;
  }
  logger_->info_log("Lua service stopped");
  return true;
}

void LuaImpl::onTick(std::time_t ms) { lua_service_->update(ms); }

void LuaImpl::onServiceCall(rpc::StubCallPtr callPtr) {
  lua_service_->call(callPtr);
}
