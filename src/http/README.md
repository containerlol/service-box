# HTTP服务器/客户端

为服务实例提供了HTTP服务器/客户端功能。

HTTP头定义:

```
using HeaderMap = std::unordered_map<std::string, std::string>;
```

HTTP请求/响应定义:
```
using HttpCallPtr = std::weak_ptr<HttpCall>;
```

主要方法分为：

1. 异步方法
```
auto do_request_async(const std::string &host, int port,
                                const std::string &uri,
                                const std::string &method,
                                const HeaderMap &headers,
                                const std::string &content, int timeout, std::uint64_t user_data,
                                ResponseHandler handler) -> bool;

using ResponseHandler = std::function<void(HttpCallPtr, std::uint64_t)>;

auto wait_request_async(const std::string &host, int port, std::uint64_t user_data,
                                  RequestHandler handler) -> bool;

using RequestHandler = std::function<bool(HttpCallPtr, HttpCallPtr, std::uint64_t)>;
```
2. 同步(协程)方法
```
auto do_request_co(const std::string &host, int port,
                             const std::string &uri, const std::string &method,
                             const HeaderMap &headers,
                             const std::string &content, int timeout) -> HttpCallPtr;

auto wait_request_co(const std::string &host, int port) -> HttpCallPtr;
```

## 使用流程

```
// 获取HTTP接口
http_ptr = getContext()->new_http();
```
```
// 在逻辑主循环内调用, ms为当前时间戳（毫秒）
http_ptr->update(ms);
```

### 异步调用

```
// 发起一个异步的HTTP请求，远程地址为:http://127.0.0.1，超时时间为2秒
http_ptr->do_request_async(
    "127.0.0.1", 80, "/", "GET", {}, "", 2, 0,
    // 异步回调
    [&](kratos::http::HttpCallPtr call, std::uint64_t user_data) {
        // 获取信息并处理
    });
```

```
// 启动一个HTTP服务器，监听地址为: http://127.0.0.1:8000
http_ptr->wait_request_async(
    "127.0.0.1", 8000, 0,
    [&](kratos::http::HttpCallPtr request, kratos::http::HttpCallPtr response, std::uint64_t) -> bool {
        response.lock()->set_content("i'm response");
        response.lock()->set_status_code(200);
        // 返回true表示本次request处理完成，返回false表示未处理完成，后续需要手动调用HttpCall::finish方法
        // 如果在HTTP超时时间（通过http.http_max_call_timeout配置）到达前仍未调用HttpCall::finish方法则
        // 本次调用将本强制终止, 此时再尝试调用response.expired方法将返回false(weak_ptr失效）
        return true;
    });
```

### 同步调用

当service box未开始协程模式时(```open_coroutine = "false"```)，同步方法将返回```empty weak_ptr```。

```
// 发起一个同步的HTTP请求，远程地址为:http://127.0.0.1, 超时时间为2秒
auto http_call = http_ptr->do_request_co("127.0.0.1", 80, "/", "GET", {}, "", 2);
// 获取信息并处理
......
```
```
// 启动一个HTTP服务器，监听地址为: http://127.0.0.1:8000
auto http_call = http.wait_request_co("127.0.0.1", 8000);
// 获取信息并处理
......
```