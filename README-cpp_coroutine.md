# C++服务的协程化调用

当service box开启协程模式时([配置](https://gitee.com/dennis-kk/service-box/blob/master/README-configuration.md))，每个用户实现的调用方法都在一个独立的协程执行，当关闭协程模式时每个用户实现的调用方法都在主线程内执行。

## 协程调用的优势
1. 避免了异步流程，原来的异步调用方式转换为"阻塞"式调用，代码流程易于理解、易于维护。
2. 协程调用与RPC结合, 调用者在进行远程通信与调用了一个本地方法类似, 不需要关注通信细节，包含互联关系，映射关系等, ```ServiceContext```暴露给开发者的是对象接口，RPC框架暴露给开发者的是服务代理接口，开发者始终在操作接口和对象方法，开发效率会获得很大的提升。

## 协程调用的缺点
1. 因为单个调用需要启动独立的协程，调用代价变高了，在service box内部也同时需要维持异步调用相同的资源(只是开发者不需要关心)，支持同样的调用并发量，资源占用量会更高。
2. 协程调用的过程，协程的错误处理可能会导致内存泄漏，service box会在一些情况下强制结束协程，现在的实现方式是让协程内部抛出一个C++标准库异常，service box内协程的包装函数会捕获异常，通过C++的对象析构函数来释放内存，虽然有一些测试用例能够覆盖能覆盖到一些异常情况，但是毕竟应用场景有限，在实际的项目开发中是否会有其他问题，需要进行验证。

## 协程调用与异步调用的编写对比
1. [异步调用](https://gitee.com/dennis-kk/service-box/blob/master/README-cpp_async_proxy_call.md)
2. [同步调用](https://gitee.com/dennis-kk/service-box/blob/master/README-cpp_sync_proxy_call.md)

以上的代码例子也能很充分的展现了不同开发模式的区别。

## ServiceContext

在服务实现类内调用```getContext```方法可以获取```ServiceContext```, ```ServiceContext```作为service box暴露给用户的功能集接口，所有service box提供给开发者使用的功能都会通过```ServiceContext```上的方法提供。

```ServiceContext```提供的几乎所有功能类接口方法和```ServiceContext```本身的方法都有异步和同步(协程)两个版本，当service box开启了协程模式时，既可以调用异步方法也可以调用同步方法，但当关闭了协程模式时，调用同步方法将会抛出C++标准异常(std::exception)。这对开发者可能会造成负担，因为一旦按照同步方法编写的实现将无法在非协程模式下运行，所有需要开发者从一开始就要确定所有服务的编写模式。另外要注意的是，协程模式的配置是针对整个service box的，一旦开启或关闭将对所有运行在这个service box上的服务同时生效。