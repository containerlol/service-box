# CMakeLists file
cmake_minimum_required(VERSION 3.5)

project (knet)

if (MSVC)
    foreach(var
        CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
        CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO
        CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
        CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO
        )
        if(${var} MATCHES "/MD")
            string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")
        endif()
    endforeach()
else()
    SET(CMAKE_C_FLAGS "-g -O2 -Wall")
endif()

include_directories(${PROJECT_SOURCE_DIR}/../knet/)

add_executable(test_client
	test_client.c
)

add_executable(test_server
	test_server.c
)

if (MSVC)
    set_target_properties(test_client PROPERTIES COMPILE_FLAGS "/EHsc /W3")
    set_target_properties(test_server PROPERTIES COMPILE_FLAGS "/EHsc /W3")
    target_link_libraries(test_client debug ${PROJECT_SOURCE_DIR}/../lib/Debug/libknet.a optimized ${PROJECT_SOURCE_DIR}/../lib/Release/libknet.a)
    target_link_libraries(test_server debug ${PROJECT_SOURCE_DIR}/../lib/Debug/libknet.a optimized ${PROJECT_SOURCE_DIR}/../lib/Release/libknet.a)
else()
    target_link_libraries(test_client libknet.a -lpthread)
    target_link_libraries(test_server libknet.a -lpthread)
endif()